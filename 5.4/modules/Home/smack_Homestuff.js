function loadAddedDiv(stuffid,stufftype){
	gstuffId = stuffid;
	new Ajax.Request(
		'index.php',
		{queue: {position: 'end', scope: 'command'},
			method: 'post',
			postBody:'module=Home&action=HomeAjax&file=NewBlock&stuffid='+stuffid+'&stufftype='+stufftype,
			onComplete: function(response){
				var responseVal=response.responseText;
				$('MainMatrix').style.display= 'none';
				$('MainMatrix').innerHTML = response.responseText + $('MainMatrix').innerHTML;
				positionDivInAccord('stuff_'+gstuffId,'',stufftype);
				initHomePage();
				loadStuff(stuffid,stufftype);

				$('MainMatrix').style.display='block';
				window.location.reload();//added by smackcoders --needed to reload graph 
			}
		}
	);
}

/**
 * this function is used to reload a widgets' content based on its id and type
 * @param string stuffid - the widget id
 * @param string stufftype - the type of the widget
 */
function loadStuff(stuffid,stufftype){
	$('refresh_'+stuffid).innerHTML=$('vtbusy_homeinfo').innerHTML;
	new Ajax.Request(
		'index.php',
		{queue: {position: 'end', scope: 'command'},
			method: 'post',
		    postBody:'module=Home&action=HomeAjax&file=HomeBlock&homestuffid='+stuffid+'&blockstufftype='+stufftype,
		    onComplete: function(response){
				var responseVal=response.responseText;

				if(stufftype!="DashBoard")//added by smackcoders
				$('stuffcont_'+stuffid).innerHTML=response.responseText;
				if(stufftype=="Module"){
					if($('more_'+stuffid).value != null && $('more_'+stuffid).value != '')
						$('a_'+stuffid).href = "index.php?module="+$('more_'+stuffid).value+"&action=ListView&viewname="+$('cvid_'+stuffid).value;
				}
				if(stufftype=="Default" && typeof($('a_'+stuffid)) != 'undefined'){
					if($('more_'+stuffid).value != ''){
						$('a_'+stuffid).style.display = 'block';
						var url = "index.php?module="+$('more_'+stuffid).value+"&action=index";
						if($('search_qry_'+stuffid)!=''){
							url += $('search_qry_'+stuffid).value;
						}
						$('a_'+stuffid).href = url;
					}else{
						$('a_'+stuffid).style.display = 'none';
					}
				}
				if(stufftype=="RSS"){
					$('a_'+stuffid).href = $('more_'+stuffid).value;
				}
				if(stufftype=="DashBoard"){	
//alert(responseVal);
					var StrippedString = responseVal.replace(/(<([^>]+)>)/ig,"");
					StrippedString = StrippedString.replace('Vertical','');
					StrippedString = StrippedString.replace('Pie','');
					StrippedString = StrippedString.replace('Horizontal','');
					var coltype = StrippedString.split('$|')[4];//for data available
					var replacecoltype = '$|'+coltype;
					StrippedString = StrippedString.replace(replacecoltype,'');
					var removeresponse = StrippedString;
					StrippedString= StrippedString.replace(/\s/g, "");
					StrippedString = StrippedString.replace(replacecoltype,'');
					replacecoltype = replacecoltype.replace(/\s+$/,"");
					//above code strips the unwanted response
					if(StrippedString.split('$|')[0]!='Nodataavailable')
					{
						var res = response.responseText;
						removeresponse = removeresponse.replace(/\s+$/,"");
						removeresponse = removeresponse.replace("var gdash_display_type = '';",'');
						res = res.replace(removeresponse,'');
						var foo = document.getElementById('stuffcont_'+stuffid);
						var canvas = document.createElement('canvas');
						if(coltype==1)
						{
							$('stuff_'+stuffid).style['width'] = "98.6%";
							canvas.setAttribute("width", 900);
							canvas.setAttribute("height", 250);
						}
						if(coltype==2)
						{
							$('stuff_'+stuffid).style['width'] = "64%";
							canvas.setAttribute("width", 600);
							canvas.setAttribute("height", 250);
						}
						if(coltype==3)
						{
							$('stuff_'+stuffid).style['width'] = "48.6%";
							canvas.setAttribute("width", 500);
							canvas.setAttribute("height", 250);
						}
						var smack_graph_type = StrippedString.split('$|')[0];  
						smack_graph_type = smack_graph_type.replace("vargdash_display_type='';",'');
						var wholelabels =StrippedString.split('$|')[2];
						var wholedata =StrippedString.split('$|')[1];
						var wholeurls = StrippedString.split('$|')[3];			
						var labels = wholelabels.split(",");
						var data = wholedata.split(",");
						var urls = wholeurls.split(",");
						var replacecolby = '<td align="left">'+replacecoltype+'</td>';
						res = res.replace(replacecolby,'');
						$('stuffcont_'+stuffid).innerHTML=res+"<a id = anchor onClick=zoomfn('"+smack_graph_type+"','"+wholelabels+"','"+wholedata+"','"+wholeurls+"') >zoom</a>";  //place contents in div 
						$('a_'+stuffid).href = "index.php?module=Dashboard&action=index&type="+$('more_'+stuffid).value;
						canvas.setAttribute("class", "mapping");
						canvas.setAttribute("style","overflow:visible;");
						canvas.setAttribute("id", stuffid);
						foo.appendChild(canvas);
						for (a in data ) {
						data[a] = parseInt(data[a]);
						}
	 					RGraph.Clear(document.getElementById(stuffid));	
						if(smack_graph_type=='Hori')
						{
						var bar = new RGraph.HBar(stuffid, data);  				
						bar.Set('chart.labels', labels);
						bar.Set('chart.colors', ['red','green','yellow','#BFCFCC','#FF7260','#336699','#ffcf75']);
						bar.Set('chart.gutter.left', 150);            
						}
						if(smack_graph_type=='verti')
						{
						var bar = new RGraph.Bar(stuffid, data);         
						var spaced_labels = new Array();
						labels_length = labels.length;
						bar.Set('chart.text.angle',45);
						bar.Set('chart.gutter.bottom', 150);
						bar.Set('chart.labels', labels);
						bar.Set('chart.colors', ['red','green','yellow','#BFCFCC','#FF7260','#336699','#ffcf75']);
						bar.Set('chart.gutter.left', 150); 
						}
						if(smack_graph_type=='pie')
						{
						var bar = new RGraph.Pie(stuffid, data);  					
						bar.Set('chart.labels', labels);
						bar.Set('chart.colors', ['red','green','yellow','#BFCFCC','#FF7260','#336699','#ffcf75']);
						bar.Set('chart.gutter.left', 150);            
						}
						if(smack_graph_type=='line')
						{
						var bar = new RGraph.Line(stuffid, data);   
						var spaced_labels = new Array();
						labels_length = labels.length;
						bar.Set('chart.labels',labels);
						bar.Set('chart.linewidth', 2);
						bar.Set('chart.tickmarks', 'circle');
						bar.Set('chart.colors', ['red','green','yellow','#BFCFCC','#FF7260','#336699','#ffcf75']);
						bar.Set('chart.gutter.left', 150); 
						bar.Set('chart.text.angle',45);
						bar.Set('chart.gutter.bottom', 150);
						}
						if(smack_graph_type=='funnel')
						{
						var bar = new RGraph.Funnel(stuffid, data);  					
						bar.Set('chart.labels', labels);
						bar.Set('chart.text.boxed', false);
						bar.Set('chart.labels.sticks', false);
						bar.Set('chart.strokestyle', 'transparent');
						bar.Set('chart.text.angle',90);
						bar.Set('chart.gutter.left', 150);            
						}
						bar.Set('chart.colors', ['red','green','yellow','blue','orange','#BFCFCC','#FF7260','#336699','#ffcf75','#443266','#8C489F']);
						var tooltiplinks = new Array();
						url_length = urls.length;
						for(var ee=0;ee<url_length;ee++)
						{
						tooltiplinks [ee] =  '<a href = "'+urls[ee]+'">'+labels[ee]+' ('+data[ee]+')</a>';
						}
						bar.Set('chart.background.grid.vlines', false);//removes the grid lines
						bar.Set('chart.background.grid.hlines', false);
						bar.Set('chart.tooltips', tooltiplinks);
						bar.Draw();	
					}
					else
					{
					 $('stuffcont_'+stuffid).innerHTML='<h2>No Data Available</h2>';
					}
				}
                if(stufftype=="ReportCharts"){
                	$('a_'+stuffid).href = "index.php?module=Reports&action=SaveAndRun&record="+$('more_'+stuffid).value;
                }
				$('refresh_'+stuffid).innerHTML='';
		    }
		}
	);
}

//this fuction shows a zoomed graph in colorbox
function zoomfn(graphtype,comlabels,comdata,comurls)
{
jQuery.colorbox({html:"<div id='zoomdiv' ><canvas id = 'zoomcanvas' width= 900 height = 400 style='overflow-x:hidden;overflow-y:hidden;'></div><script></script> ",

onComplete:function(){

var data = comdata.split(","); //forms an array for graph
var labels = comlabels.split(",");
var urls = comurls.split(",");
var tooltiplinks = new Array();
for (a in data ) {
data[a] = parseInt(data[a]);
}

RGraph.Clear(document.getElementById('zoomcanvas'));
if(graphtype=='Hori')
{
var bar1 = new RGraph.HBar('zoomcanvas', data); 
}
if(graphtype=='verti')
{
var bar1 = new RGraph.Bar('zoomcanvas', data); 
}
if(graphtype=='pie')
{
var bar1 = new RGraph.Pie('zoomcanvas', data); 
}
if(graphtype=='line')
{
var bar1 = new RGraph.Line('zoomcanvas', data); 
bar1.Set('chart.linewidth', 5);
bar1.Set('chart.tickmarks', 'circle');
}
if(graphtype=='funnel')
{
var bar1 = new RGraph.Funnel('zoomcanvas', data); 
bar1.Set('chart.labels.sticks', true);
bar1.Set('chart.strokestyle', 'transparent');
bar1.Set('chart.gutter.left', 100);            
}

bar1.Set('chart.labels',labels);
bar1.Set('chart.colors', ['red','green','yellow','#BFCFCC','#FF7260','#336699','#ffcf75']);

url_length = urls.length;
for(var ee=0;ee<url_length;ee++)
{
tooltiplinks[ee] =  '<a href = "'+urls[ee]+'">'+labels[ee]+' ('+data[ee]+')</a>';
}
bar1.Set('chart.events.mousemove', function (e, bar) {e.target.style.cursor = 'pointer';});
bar1.Set('chart.tooltips', tooltiplinks);//adds links 2 graph
bar1.Set('chart.background.grid.vlines', false);//removes the grid lines
bar1.Set('chart.background.grid.hlines', false);
bar1.Draw();	

},
onClosed:function(){
RGraph.Clear(document.getElementById('zoomcanvas'));	
}
});

		
}
function fetch_homeDB(stuffid){
	$('refresh_'+stuffid).innerHTML=$('vtbusy_homeinfo').innerHTML;
	new Ajax.Request(
		'index.php',
		{queue: {position: 'end', scope: 'command'},
			method: 'post',
			postBody: 'module=Dashboard&action=DashboardAjax&file=HomepageDB',
			onComplete: function(response){

				/*$('stuffcont_'+stuffid).style.display = 'none';
				$('stuffcont_'+stuffid).innerHTML=response.responseText;
				$('refresh_'+stuffid).innerHTML='';
				Effect.Appear('stuffcont_'+stuffid);*/
				responseVal = response.responseText;
				$('stuffcont_'+stuffid).style.display = 'none';
				//$('stuffcont_'+stuffid).innerHTML=response.responseText;
				//$('refresh_'+stuffid).innerHTML='';
				//Effect.Appear('stuffcont_'+stuffid);
				var StrippedString,responseVal;
				StrippedString= responseVal.replace(/\s/g, "");

				var wholedata =StrippedString.split('$|')[0];
				var wholelabels =StrippedString.split('$|')[1];
				var wholeurls = StrippedString.split('$|')[2];
				var labels = wholelabels.split("::");
				var data = wholedata.split("::");
				var urls = wholeurls.split("::");
				var foo = document.getElementById('stuff_'+stuffid);
				$('stuff_'+stuffid).style['width'] = "98.6%";						
				var canvas = document.createElement('canvas');
				canvas.setAttribute("width", 900);
				canvas.setAttribute("height", 250);
				canvas.setAttribute("class", "mapping");
				canvas.setAttribute("style","overflow:visible;");
				canvas.setAttribute("id", stuffid);
				foo.appendChild(canvas);
				for (a in data ) {
				data[a] = parseInt(data[a]);
				}
	 			RGraph.Clear(document.getElementById(stuffid));	
				var bar = new RGraph.HBar(stuffid, data);  	
				bar.Set('chart.labels', labels);
				bar.Set('chart.colors', ['red','green','yellow','#BFCFCC','#FF7260','#336699','#ffcf75']);
				bar.Set('chart.gutter.left', 150);  
				var tooltiplinks = new Array();
				url_length = urls.length;
				for(var ee=0;ee<url_length;ee++)
				{
					urls[ee] = decodeURIComponent(urls[ee]);
					tooltiplinks [ee] =  '<a href = "'+urls[ee]+'">'+labels[ee]+' ('+data[ee]+')</a>';
				}
				bar.Set('chart.background.grid.vlines', false);//removes the grid lines
				bar.Set('chart.background.grid.hlines', false);
				bar.Set('chart.tooltips', tooltiplinks);
				bar.Draw();
			}
		}
	);
}
