<?php 
require_once('config.php');
require_once('include/utils/GraphUtils.php');
include_once ('Image/Graph.php');
include_once ('Image/Canvas.php');

function othergraph_values($referdata,$refer_code,$width,$height,$left,$right,$top,$bottom,$title,$target_val,$cache_file_name,$html_image_name,$graphtype)
{

	global $log,$root_directory,$lang_crm,$theme;
//We'll be getting the values in the form of a string separated by commas
	$datay=explode("::",$referdata); // The datay values  
	$datax=explode("::",$refer_code); // The datax values  

// The links values are given as string in the encoded form, here we are decoding it
	$target_val=urldecode($target_val);
	$target=explode("::",$target_val);

	$alts=array();
	$temp=array();
	for($i=0;$i<count($datax);$i++)
	{
		$name=$datax[$i];
		$pos = substr_count($name," ");
		$alts[]=htmlentities($name)."=%d";
//If the daatx value of a string is greater, adding '\n' to it so that it'll cme inh 2nd line
		 if(strlen($name)>=14)
                        $name=substr($name, 0, 44);
		if($pos>=2)
		{
			$val=explode(" ",$name);
			$n=count($val)-1;

			$x="";
			for($j=0;$j<count($val);$j++)
			{
				if($j != $n)
				{
					$x  .=" ". $val[$j];
				}
				else
				{
					$x .= "@#".$val[$j];
				}
			}
			$name = $x;
		}
		$name=str_replace("@#", "\n",$name);
		$temp[]=$name; 
	}
	$datax=$temp;
	$max=0;
$dataset = & Image_Graph::factory('dataset');
for($i=0;$i<count($datay); $i++)
	{
		$x=1+2*$i;
		if($datay[$i]>=$max) $max=$datay[$i];
		$dataset->addPoint(
			        $x,
			        $datay[$i],
			        array(
			            'url' => $target[$i],
			            'alt' => $alts[$i]
			        )
	    );
// build the xaxis label array to allow intermediate ticks

		$xlabels[$x] = $datax[$i];
		$xlabels[$x+1] = '';

		// To have unique names even in case of duplicates let us add the id
		$datax_appearance = $uniquex[$datax[$i]];
		if($datax_appearance == null) {
			$uniquex[$datax[$i]] = 1;			
		} else {
			$xlabels[$x] = $datax[$i] .' ['. $datax_appearance.']';
			$uniquex[$datax[$i]] = $datax_appearance + 1;			
		}
	}
	$dataset_ret = array_merge((array)$dataset , $datax);


$test_whether = graph_generate($dataset_ret,$graphtype);
return $test_whether;
}
function graph_generate($values_get,$smack_graph_type){

	$x_values = null;
	$y_values = null;
$url = null;
	$smack_count = count($values_get['_data']);
	for($i=0;$i<$smack_count;$i++)
	{
		if($i==0)
		{
			$x_values.= $values_get['_data'][$i]['Y'];
			$y_values .= $values_get[$i];
			$url .= $values_get['_data'][$i]['data']['url'];

		}
		else if($i==$smack_count-1)
		{
			$x_values.=','.$values_get['_data'][$i]['Y'];
			$y_values.=','.$values_get[$i];
			$url.=','.$values_get['_data'][$i]['data']['url'];
		}
		else
		{
			$x_values.= ','.$values_get['_data'][$i]['Y'];
			$y_values.= ','.$values_get[$i];
			$url.= ','.$values_get['_data'][$i]['data']['url'];
		}

	}
$x_values = $x_values.'$|'.$y_values;
$x_values = $smack_graph_type.'$|'.$x_values;
$x_values = $x_values.'$|'.$url;
	echo $x_values;

}
?>

