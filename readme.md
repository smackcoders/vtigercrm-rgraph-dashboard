# VtigerCRM Dashboard #

## Instruction ##

1.Extract vt-dashboard.tar.gz in the vtigercrm root directory.
2.Add a column in vtiger_homedashbd table.
Alter table vtiger_homedashbd add noofcolumns int(5) Default 1;

## Included 3rd Party libraries ##
Colorbox version 1.3.19.3
RGraph 2_2012-07-26-stable


## Features ##
## Version 0.9 beta ##

1. Adds a compact way to view data graphically.
2. Graphs can be viewed in three column sizes in home page.
3. Five charts are implemented to represent graphical data.
  *  Horizontal bar chart
  *  Vertical bar chart
  *  Pie chart
  *  Line chart
  *  Funnel chart
4.Zoom option shows a zoomed graph in which datas can be analysed more clearly.


## Contributors

- Smackcoders Team
